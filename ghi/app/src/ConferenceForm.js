import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }

    const handleStartsChange = (e) => {
        const value = e.target.value;
        setStarts(value);
    }

    const handleEndsChange = (e) => {
        const value = e.target.value;
        setEnds(value);
    }

    const handleDescriptionChange = (e) => {
        const value = e.target.value;
        setDescription(value);
    }

    const handleMaxPresentationsChange = (e) => {
        const value = e.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendeesChange = (e) => {
        const value = e.target.value;
        setMaxAttendees(value);
    }

    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value);
    }

    console.log(location)


    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        console.log(JSON.stringify(data))

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation)

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data.locations)
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input placeholder="Name" value={name} onChange={handleNameChange} required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="mm/dd/yyyy" value={starts} onChange={handleStartsChange} required type="date" name="starts" id="starts" className="form-control"/>
                                <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="mm/dd/yyyy" value={ends} onChange={handleEndsChange} required type="date" name="ends" id="ends" className="form-control"/>
                                <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <textarea className="form-control" value={description} onChange={handleDescriptionChange} name="description" id="description" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Maximum Presentations" value={maxPresentations} onChange={handleMaxPresentationsChange} required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                                <label htmlFor="max_presentations">Maximum Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Maximum Attendees" value={maxAttendees} onChange={handleMaxAttendeesChange} required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                                <label htmlFor="max_attendees">Maximum Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select required value={location} onChange={handleLocationChange} id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map((location) => {
                                    return (
                                        <option value={location.id} key={location.id}>
                                            {location.name}
                                        </option>
                                );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ConferenceForm
